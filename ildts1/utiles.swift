//
//  utiles.swift
//  iLDTS-3
//
//  Created by Josemaria Carazo Abolafia on 27/09/2019.
//  Copyright © 2019 Josemaria Carazo Abolafia. All rights reserved.
//

import Foundation

class utiles {

    static func cleandate(fecha: Date) -> Date {
        let fechaok =
            string2dateus(fecha:
                dateus2string(fechadate: fecha))
        return fechaok
    }
    static func string2dateus(fecha: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale.current //WhereEverIam
        let fechadate = dateFormatter.date(from:fecha)!
        return fechadate
    }
    static func dateus2string(fechadate: Date) -> String {
        let calendario = Calendar.current
        let year2=calendario.component(.year, from: fechadate)
        let month2=calendario.component(.month, from: fechadate)
        let day2=calendario.component(.day, from: fechadate)
        return "\(year2)" + "-" + "\(month2)" + "-" + "\(day2)"
    }
    
    static func IntDiv(num: Int, dvsr: Int) -> Int {
        // performs integer division of num/dvsr - eg IntDiv(9,4)=2
        var negate = false
        var result = 0
        var num2 = num
        var dvsr2 = dvsr
        
        if (dvsr == 0) {
            return -1
        }
        else {
            if (num * dvsr < 0 ) {
                negate = true
            }
            if (num < 0) {
                num2 = -num
            }
            if (dvsr < 0) {
                dvsr2 = -dvsr
            }
            result = ((num2 - (num2 % dvsr2)) / dvsr2)
            if (negate) {
                return -result
            } else {
                return result
            }
        }
    }
    
    static func getDayOfWeek(fecha: Date) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: fecha)
        guard let todayDate = formatter.date(from: result) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        var weekDay = myCalendar.component(.weekday, from: todayDate)
        //Domingo 1, Sabado 7 -- cambiamos a la española: Lunes 1, Domingo 7
        weekDay -= 1
        if (weekDay==0) {
            weekDay = 7
        }
        return weekDay
    }
    
    static func nSemana(desde: Date, hasta: Date) -> Int {
        let diasTO = Calendar.current.dateComponents([.day], from: desde, to: hasta)
        return utiles.IntDiv(num: diasTO.day!, dvsr: 7) + 1
    }
    
    static func nSemana(desde: String, hasta: Date) -> Int {
        let diasTO = Calendar.current.dateComponents([.day], from: utiles.string2dateus(fecha: desde), to: hasta)
        return utiles.IntDiv(num: diasTO.day!, dvsr: 7) + 1
    }
    
    static func str2int(numero: String) -> Int {
        return (numero as NSString).integerValue
    }
    
    static func fecha0esmayor(fecha0: String, fecha2: String) -> Bool {
        let f0ok = string2dateus(fecha: fecha0)
        return fecha0esmayor(fecha0: f0ok, fecha2: fecha2)
    }
    static func fecha0esmayor(fecha0: Date, fecha2: String) -> Bool {
        let f2ok = string2dateus(fecha: fecha2)
        return fecha0esmayor(fecha0: fecha0, fecha2: f2ok)
    }
    static func fecha0esmayor(fecha0: Date, fecha2: Date) -> Bool {
        var mayor = false
        
        //var diferencia = (fecha0.timeIntervalSince(fecha2)/3600) //en horas
        mayor = fecha0 > fecha2
                
        return mayor
    }

    static func fecha0esmayoroigual(fecha0: String, fecha2: String) -> Bool {
        let f0ok = string2dateus(fecha: fecha0)
        return fecha0esmayoroigual(fecha0: f0ok, fecha2: fecha2)
    }
    static func fecha0esmayoroigual(fecha0: Date, fecha2: String) -> Bool {
        let f2ok = string2dateus(fecha: fecha2)
        return fecha0esmayoroigual(fecha0: fecha0, fecha2: f2ok)
    }
    static func fecha0esmayoroigual(fecha0: Date, fecha2: Date) -> Bool {
        var mayor = false
        
        mayor = fecha0 >= fecha2
 
        return mayor
    }


}
